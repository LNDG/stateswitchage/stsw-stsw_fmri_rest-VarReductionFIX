#!/bin/bash

# apply transformation to MNI space to the variance-decrease-through-FIX maps

SUBJECTS_IDS=$(ls /Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/rest/preproc/B_data/D_preproc/) # get all subject IDs

for subject in ${SUBJECTS_IDS[@]} ; do

	root="/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri"
	xfmIn="${root}/rest/preproc/B_data/D_preproc/${subject}/preproc/rest/FEAT.feat/reg/${subject}_rest_preproc_to_MNI.mat"
	refIn="${root}/rest/analyses/A_VarReductionFIX/B_data/allSubs_mean_fixRedux_MNI_rel.nii.gz"
	FIXVarIn="${root}/rest/analyses/A_VarReductionFIX/B_data/${subject}_SNR.nii.gz"
	out="${root}/rest/analyses/A_VarReductionFIX/B_data/${subject}_SNR_MNI_rel.nii.gz"
	
	flirt -in ${FIXVarIn} \
		-ref ${refIn} \
		-applyxfm -init ${xfmIn} \
		-out ${out}

done