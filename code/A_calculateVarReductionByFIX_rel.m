% 171114 JQK: This script calculates the variance that was removed by
% applying FIX. Note that these are all still in the individual subject
% space.

% 171218 | added percentage variance reduction
% 180220 | adpated for STSWD YA

% N = 43 (1126 has no Rest data)
IDs = {'1117';'1118';'1120';'1124';'1125';'1131';'1132';'1135';'1136';'1151';'1160';'1164';'1167';'1169';'1172';'1173';'1178';'1182';'1214';'1215';'1216';'1219';'1223';'1227';'1228';'1233';'1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';'1261';'1265';'1266';'1268';'1270';'1276';'1281'};

for indID = 1:numel(IDs)
    
    ID = IDs{indID}; disp(ID);
    pn.subjFolder = ['/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/rest/preproc/B_data/D_preproc/',ID,'/preproc/rest/'];
    pn.preFix = [pn.subjFolder, ID, '_rest_feat_detrended_bandpassed.nii.gz'];
    pn.postFix = [pn.subjFolder, ID, '_rest_feat_detrended_bandpassed_manualdenoise.nii.gz'];
    pn.outFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/rest/analyses/A_VarReductionFIX/B_data/';
    
    addpath(genpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/rest/analyses/A_VarReductionFIX/D_tools/'));

    preFix = load_untouch_nii(pn.preFix);
    postFix = load_untouch_nii(pn.postFix);

    % compare voxel-wise variability between the two images
    varPreFix = std(preFix.img,[],4);
    varPostFix = std(postFix.img,[],4);
    varPreMinPost = varPreFix-varPostFix;
    varPreMinPost_Rel = (varPreFix-varPostFix)./varPreFix;
    varPreMinPost_Rel(isnan(varPreMinPost_Rel)) = 0;
    
    % save difference as nifty
    % note that the headers will be messed up, as we go from 4D to 3D data
    % without adjustment; FIX:just copy everything across 4-dimensions. We
    % can't screw around with the header as we still need to apply the
    % coreg matrix, which I believe is 4D (?).
    preFix.img = repmat(varPreMinPost_Rel, 1,1,1,size(postFix.img,4));
    save_untouch_nii(preFix, [pn.outFolder,ID,'_fixRedux_rel.nii.gz']);
    
    MergedMatPrePost(indID,:,:,:) = varPreMinPost_Rel(:,:,:,1);
end % ID loop

preFix.img = squeeze(nanmean(MergedMatPrePost,1));
save_untouch_nii(preFix, [pn.outFolder, 'allSubs_mean_fixRedux_rel.nii.gz']);

preFix.img = squeeze(nanstd(MergedMatPrePost,[],1));
save_untouch_nii(preFix, [pn.outFolder, 'allSubs_var_fixRedux_rel.nii.gz']);
    