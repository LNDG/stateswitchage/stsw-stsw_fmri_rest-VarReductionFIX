% 180221 | function adapted from MerlinDynamics for STSWD YA

% N = 43 (1126 has no Rest data)
IDs = {'1117';'1118';'1120';'1124';'1125';'1131';'1132';'1135';'1136';'1151';'1160';'1164';'1167';'1169';'1172';'1173';'1178';'1182';'1214';'1215';'1216';'1219';'1223';'1227';'1228';'1233';'1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';'1261';'1265';'1266';'1268';'1270';'1276';'1281'};

pn.root             = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/rest/';
pn.data             = [pn.root, 'analyses/A_VarReductionFIX/B_data/'];
pn.tools            = [pn.root, 'preproc/D_tools/']; addpath(genpath(pn.tools)); % add toolboxes

MergedMatPrePost = [];
for indID = 1:numel(IDs)
    ID = IDs{indID}; disp(ID);
    try
        pn.fixReduxMNI = [pn.data, ID, '_SNR_MNI_rel.nii.gz'];
        fixReduxMNI = load_untouch_nii(pn.fixReduxMNI);
    catch
        pn.fixReduxMNI = [pn.data, ID, '_SNR_MNI_rel.nii'];
        fixReduxMNI = load_untouch_nii(pn.fixReduxMNI);
    end
    MergedMatPrePost(indID,:,:,:) = squeeze(fixReduxMNI.img(:,:,:,1));
end % ID loop

figure; 
for indID = 1:size(MergedMatPrePost,1)
    disp(num2str(indID));
    subplot(7,10, indID);
    imagesc(squeeze(MergedMatPrePost(indID,10,:,:)));
end

fixReduxMNI.img = squeeze(nanmean(MergedMatPrePost,1));
save_untouch_nii(fixReduxMNI, [pn.data, 'allSubs_mean_SNR_MNI_rel.nii.gz']);

fixReduxMNI.img = squeeze(nanstd(MergedMatPrePost,[],1));
save_untouch_nii(fixReduxMNI, [pn.data, 'allSubs_var_SNR_MNI_rel.nii.gz']);

fixReduxMNI.img = squeeze(nanstd(MergedMatPrePost,[],1))./squeeze(nanmean(MergedMatPrePost,1));
fixReduxMNI.img(isnan(fixReduxMNI.img)) = 0;
save_untouch_nii(fixReduxMNI, [pn.data, 'allSubs_cov_SNR_MNI_rel.nii.gz']);
