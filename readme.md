## A_VarReductionFIX

Curator: JQK

Purpose: Create individual and group-level images of the relative variance reduction by FIX.

## Scripts

A_calculateVarReductionByFIX_rel

- N = 43 YAs (1126 no rest)
- Load images pre-and post v1 FIX preproc
- Calculate relative change for pre to post ((pre-post)/pre)
- Save individual image

B_registerToMNI_rel.sh

- Apply FLIRT-estimated transformation matrix to MNI on individual reduction images

C_avgVarReduction_MNI_rel

- Calculate mean, std and cov across individual variance reduction images
